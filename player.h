//
//  player.h
//  Starfox
//
//  Created by Julius Hetzel on 26.05.15.
//  Copyright (c) 2015 de.hsb.JUTOKE. All rights reserved.
//

#ifndef Starfox_player_h
#define Starfox_player_h
#define head_coords_amount 10
#define body_coords_amount 19


#include <stdio.h>
#include <GLFW/glfw3.h>
#include <vector>
#include "vec3.hpp"
#include "sfTriangle.h"

#endif /* defined(Starfox_player_h) */

namespace starfox {// namespace graphics {
//	class sfTriangle;
    class Player{
    private:
        Vec3 forward;
        Vec3 up;
        Vec3 right;
        Vec3 origin;
        Vec3 heavy;
        const char* player_name;
        std::vector<Vec3> points;

        void initPoints();
        void SetMaterialColor(int side, double r, double g, double b);
        void moveForward(double speed);

        double currentXAngle;
        double currentYAngle;
        double currentZAngle;

        double zSpeed;
        double yRot = 0.02;
        double xRot;
    
    public:
        Player(const char *name);
        ~Player();


        std::vector<starfox::sfTriangle> triangles;
        int trianglesAmount;
        
        void draw_body();
        void draw_triangle(int p1, int p2, int p3);


        void rotateAroundOwnZ(double angle);
        void rotateAroundOwnY(double angle);
        void rotateAroundOwnX(double angle);

        double getZSpeed();
        double getZValue();

        void movePlayer(double xAngle, double yAngle, double zAngle, double speed);
        void translate(double x, double y, double z);

    };
}//}
