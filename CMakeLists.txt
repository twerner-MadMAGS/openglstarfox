cmake_minimum_required(VERSION 3.2)
project(openglstarfox)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES
    globals.h
    main.cpp
    player.cpp
    player.h
    vec3.cpp
    vec3.hpp
    window.cpp
    window.h
    point3.cpp
    point3.h
    area.cpp
    area.h
    randomizer.cpp
    randomizer.h
    Mat3.cpp
    Mat3.h)

add_executable(openglstarfox ${SOURCE_FILES})

find_package(opengl REQUIRED)

include_directories(/usr/local/lib)

set(CMAKE_FIND_FRAMEWORK "ONLY")

find_library(OPENGL_LIBRARY opengl)

message("STATUS ${OPENGL_LIBRARY}")
target_link_libraries(openglstarfox
glfw3 ${OPENGL_LIBRARY})
