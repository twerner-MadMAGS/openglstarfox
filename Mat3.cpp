//
// Created by Julius Hetzel on 14.06.15.
//

#include <ostream>
#include <iostream>
#include "Mat3.h"
#include "vec3.hpp"

Mat3::Mat3(){}

Mat3::Mat3(double a0, double a1,double a2,double a3,double a4,double a5,double a6,double a7,double a8){
    p[0][0] = a0;
    p[0][1] = a1;
    p[0][2] = a2;
    p[1][0] = a3;
    p[1][1] = a4;
    p[1][2] = a5;
    p[2][0] = a6;
    p[2][1] = a7;
    p[2][2] = a8;
}

void Mat3::printMat() {
    printf("%f", p[0][0]);
    std::cout << "  ";
    printf("%f", p[0][1]);
    std::cout << "  ";
    printf("%f", p[0][2]);
    std::cout << std::endl;

    printf("%f", p[1][0]);
    std::cout << "  ";
    printf("%f", p[1][1]);
    std::cout << "  ";
    printf("%f", p[1][2]);
    std::cout << std::endl;

    printf("%f", p[2][0]);
    std::cout << "  ";
    printf("%f", p[2][1]);
    std::cout << "  ";
    printf("%f", p[2][2]);
    std::cout << std::endl;
}

Mat3& Mat3::operator*=(Mat3 a) {
    Mat3 m;

    for(int i = 0 ; i < 4 ; i++){
        for(int j = 0 ; j < 3 ; j++){
            double temp = 0.0;
            for(int c = 0 ; c < 3 ; c++){
                temp += p[i][c]*a.p[c][j];
            }
            m.p[i][j] = temp;
        }
    }
    return m;
}


Mat3::Mat3(double m[3][3]){
    for(int i = 0 ; i < 3 ; i++) {
        for (int k = 0; k < 3; k++) {
            p[i][k] = m[i][k];
        }
    }
}


Vec3 Mat3::operator*=(Vec3& v) {
    Vec3 ret;
    ret.p[0] = p[0][0]*v.p[0] + p[0][1]*v.p[1] + p[0][2]*v.p[2];
    ret.p[1] = p[1][0]*v.p[0] + p[1][1]*v.p[1] + p[1][2]*v.p[2];
    ret.p[2] = p[2][0]*v.p[0] + p[2][1]*v.p[1] + p[2][2]*v.p[2];
    return ret;
}