//
// Created by Julius Hetzel on 11.06.15.
//

#include "point3.h"

double Point::distanceToPoint(Point p){
    return (p - *this).Length();
}