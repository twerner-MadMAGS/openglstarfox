//
// Created by Julius Hetzel on 11.06.15.
//

#ifndef OPENGLSTARFOX_POINT3_H
#define OPENGLSTARFOX_POINT3_H


#include "vec3.hpp"

class Point {
public:
    double x, y, z;
    Point(double x = 0, double y = 0, double z = 0): x(x), y(y), z(z) {}
    Point operator +(Vec3 v) {return Point(x + v.getCoordinate(0), y + v.getCoordinate(1), z + v.getCoordinate(2));}
    Point& operator +=(Vec3 v) {x += v.getCoordinate(0); y += v.getCoordinate(1); z += v.getCoordinate(2); return *this;}
    Vec3 operator -(Point p) {return Vec3(x - p.x, y - p.y, z - p.z);}
    double distanceToPoint(Point p);
};


#endif //OPENGLSTARFOX_POINT3_H
