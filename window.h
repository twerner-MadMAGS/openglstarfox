//
//  window.h
//  Starfox
//
//  Created by Julius Hetzel on 26.05.15.
//  Copyright (c) 2015 de.hsb.JUTOKE. All rights reserved.
//

#ifndef Starfox_window_h
#define Starfox_window_h
#include <GLFW/glfw3.h>
namespace starfox { namespace graphics {
    
    class Window{
        
    private:
        const char *m_name_; //m_ for member (private)
        GLFWwindow *m_window;
        
        bool init();
        
    public:
        Window(const char *name, int width, int height);
        ~Window();
        
        int window_width, window_height;
        
        bool closed() const;
        void update() const;
        void clear() const;
    };
}}

#endif
