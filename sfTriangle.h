//
//  sfTriangle.h
//  Starfox
//
//  Created by Julius Hetzel on 26.05.15.
//  Copyright (c) 2015 de.hsb.JUTOKE. All rights reserved.
//

#ifndef Starfox_sfTriangle_h
#define Starfox_sfTriangle_h

#include <stdio.h>
#include <GLFW/glfw3.h>
#include <vector>
#include "vec3.hpp"

#endif /* defined(Starfox_sfTriangle_h) */

namespace starfox {
    class sfTriangle{
    public:
    	sfTriangle(Vec3 p1, Vec3 p2, Vec3 p3, Vec3 norm);
        ~sfTriangle();
        
        std::vector<Vec3> points;
        Vec3 normal;

        void draw();
    };
}
