//
//  window.cpp
//  Starfox
//
//  Created by Julius Hetzel on 26.05.15.
//  Copyright (c) 2015 de.hsb.JUTOKE. All rights reserved.
//

#include "window.h"
#include "globals.h"
#include <iostream>

namespace starfox { namespace graphics {
    Window::Window(const char *name, int width, int height){
        m_name_ = name;
        window_height = height;
        window_width = width;
        if(!init()){
            glfwTerminate();
        }
    }
    
    Window::~Window(){
        glfwTerminate();
    }
    
    bool Window::init(){
        if(!glfwInit()){
            LOG("error");
            return true;
        }else{
            LOG("success");
        }
        
        m_window = glfwCreateWindow(window_width, window_height,"Starfox", NULL, NULL);
        if(!m_window){
            glfwTerminate();
            LOG("Window failed");
            return false;
        }
        glfwMakeContextCurrent(m_window);
        return true;
    }
    
    bool Window::closed() const {
        return glfwWindowShouldClose(m_window);
    }
    
    void Window::clear() const {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    
    void Window::update() const{
        glfwPollEvents();
        glfwSwapBuffers(m_window);
    }
}}