//
//  player.cpp
//  Starfox
//
//  Created by Julius Hetzel on 26.05.15.
//  Copyright (c) 2015 de.hsb.JUTOKE. All rights reserved.
//

#include <iostream>
#include "player.h"
#include "Mat3.h"

#define DRAWPOINT(t) glVertex3f(points[t].p[0], points[t].p[1], points[t].p[2])


namespace starfox { //namespace graphics {
    
	Player::Player(const char *name){
		player_name = name;
		trianglesAmount = 0;
		//points = new std::vector<Vec3>();
		Player::initPoints();
        origin = *new Vec3(0,0,-12.5);
        forward = *new Vec3(0,0,-13.5);
        up = *new Vec3(0,1,-12.5);
        right = *new Vec3(1,0,-12.5);
        currentYAngle = 0.0;
        currentXAngle = 0.0;
        currentZAngle = 0.0;

	}

	Player::~Player(){
	}
    
	void Player::initPoints() {
		//testpoints
		//points.push_back(*new Vec3(-6, 0, 0));
		//points.push_back(*new Vec3(6, 0, 0));
		//points.push_back(*new Vec3(6, 6, 0));

		points.push_back(*new Vec3(0, 0, -25));
		points.push_back(*new Vec3(-2, 1.8, -16));
		points.push_back(*new Vec3(2, 1.8, -16));
		points.push_back(*new Vec3(-3, 0, -22));
		points.push_back(*new Vec3(3, 0, -22));
		points.push_back(*new Vec3(-4.5, 0, -13));
		points.push_back(*new Vec3(4.5, 0, -13));
		points.push_back(*new Vec3(-4, 2, -9));
		points.push_back(*new Vec3(4, 2, -9));
		points.push_back(*new Vec3(-5, 0, -5));
		points.push_back(*new Vec3(5, 0, -5));
		points.push_back(*new Vec3(-3.5, 0.2, 0));
		points.push_back(*new Vec3(3.5, 0.2, 0));
		points.push_back(*new Vec3(0, -2, -9));
		points.push_back(*new Vec3(0, -2, -18));
		points.push_back(*new Vec3(15, -1, -16));
		points.push_back(*new Vec3(19, 0, -7));
		points.push_back(*new Vec3(-15, -1, -16));
		points.push_back(*new Vec3(-19, 0, -7));

        points.push_back(*new Vec3(0,0,0)); //Ursprung
        points.push_back(*new Vec3(0,1,0)); //y-axis
        points.push_back(*new Vec3(0,0,1)); //z-axis
        points.push_back(*new Vec3(1,0,0)); //x-axis
	}

    double Player::getZValue(){
        return points[20].p[2]*-1;
    }

    void SetMaterialColor(int side, double r, double g, double b) {
      float	amb[4], dif[4], spe[4];
      int mat;

      dif[0] = r;
      dif[1] = g;
      dif[2] = b;

      for(int i = 0; i < 3; i++) {
        amb[i] = .1 * dif[i];
        spe[i] = .5;
      }
      amb[3] = dif[3] = spe[3] = 1.0;

      switch(side){
        case 1:	mat = GL_FRONT;
          break;
        case 2:	mat = GL_BACK;
          break;
        default: mat = GL_FRONT_AND_BACK;
      }

      glMaterialfv(mat, GL_AMBIENT, amb);
      glMaterialfv(mat, GL_DIFFUSE, dif);
      glMaterialfv(mat, GL_SPECULAR, spe);
      glMaterialf( mat, GL_SHININESS, 20);
    }
    
    void Player::draw_triangle(int p1, int p2, int p3) {
    	glBegin(GL_TRIANGLES);
        //coss
        Vec3 v1 = *new Vec3(points[p2] - points[p1]);
        Vec3 v2 = *new Vec3(points[p3] - points[p1]);

        Vec3 cross = Vec3(0, 0, 0);
        cross.p[0] = (v1.p[1] * v2.p[2]) - (v1.p[2] * v2.p[1]);
        cross.p[1] = (v1.p[2] * v2.p[0]) - (v1.p[0] * v2.p[2]);
        cross.p[2] = (v1.p[0] * v2.p[1]) - (v1.p[1] * v2.p[0]);

        float crossL = sqrt(pow(cross.p[0], 2) + pow(cross.p[1], 2) + pow(cross.p[2], 2));
        cross.p[0] = cross.p[0] / crossL;
        cross.p[1] = cross.p[1] / crossL;
        cross.p[2] = cross.p[2] / crossL;

        //glNormal3f(0, 0, 1);
        DRAWPOINT(p1);
        DRAWPOINT(p2);
        DRAWPOINT(p3);
        glNormal3dv(cross.p);

        Vec3* vec1 = new Vec3(points[p1].p[0], points[p1].p[1], points[p1].p[2]);
        Vec3* vec2 = new Vec3(points[p2].p[0], points[p2].p[1], points[p2].p[2]);
        Vec3* vec3 = new Vec3(points[p3].p[0], points[p3].p[1], points[p3].p[2]);
        starfox::sfTriangle* t = new sfTriangle(*vec1, *vec2, *vec3, cross);
        triangles.push_back(*t);
        trianglesAmount++;

        glEnd();
    }
    
    void Player::draw_body(){
        //SetMaterialColor(0, 0, 0, 1.0);

        //testtriangle
        //draw_triangle(0, 1, 2);

        //front_top
        draw_triangle(0, 1, 2);

        draw_triangle(0, 3, 1);
        draw_triangle(0, 2, 4);
        draw_triangle(1, 3, 5);
        draw_triangle(2, 6, 4);
        
        //middle_side and back
        draw_triangle(1, 5, 7);
        draw_triangle(2, 8, 6);
        draw_triangle(5, 9, 7);
        draw_triangle(6, 8, 10);
        draw_triangle(7, 9, 11);
        draw_triangle(8, 12, 10);
        draw_triangle(1, 7, 2);
        draw_triangle(2, 7, 8);
        draw_triangle(7, 11, 8);
        draw_triangle(8, 11, 12);

        //bottom
        draw_triangle(3, 0, 14);
        draw_triangle(0, 4, 14);
        draw_triangle(3, 14, 13);
        draw_triangle(4, 13, 14);
        draw_triangle(4, 6, 13);
        draw_triangle(5, 3, 13);
        draw_triangle(9, 5, 13);
        draw_triangle(6, 10, 13);
        draw_triangle(11, 9, 13);
        draw_triangle(10, 12, 13);
        draw_triangle(12, 11, 13);

        //right wing
        	//top
        draw_triangle(2, 15, 4);
        draw_triangle(2, 8, 15);
        draw_triangle(8, 16, 15);
        draw_triangle(8, 10, 16);
        	//bottom
        draw_triangle(4, 15, 14);
        draw_triangle(15, 13, 14);
        draw_triangle(15, 16, 13);
        draw_triangle(16, 10, 13);

        
        //left wing

        	//top
        draw_triangle(1, 17, 13);
        draw_triangle(1, 17, 7);
        draw_triangle(7, 18, 17);
        draw_triangle(7, 18, 9);
        	//bottom
        draw_triangle(17, 3, 14);
        draw_triangle(13, 17, 14);
        draw_triangle(18, 17, 13);
        draw_triangle(9, 18, 13);
    }

    void Player::translate(double x, double y, double z){
            Vec3 v = *new Vec3(x,y,z);
            for(int i = 0 ; i < points.size() ; i++){
                points.at(i) += v;
            }
            up += v;
            forward += v;
            origin += v;
            right += v;
    }

        void Player::moveForward(double speed){
            Vec3 dir = (forward - origin) * speed;
            zSpeed = dir.p[2];
            translate(dir.p[0], dir.p[1], dir.p[2]);
        }

        double Player::getZSpeed() {
            //printf("%f",zSpeed);
            return zSpeed;
        }

        void Player::rotateAroundOwnY(double angle) {
            angle = angle * (3.141592635 / 180);
            currentYAngle += angle;
            Vec3 o = origin;
            translate(-origin.p[0],-origin.p[1],-origin.p[2]);

            double n1 = up.p[0];
            double n2 = up.p[1];
            double n3 = up.p[2];

            double delA = 1-cos(angle);
            double cosA = cos(angle);
            double sinA = sin(angle);

            Mat3 rot(n1*n1*delA+cosA, n1*n2*delA-n3*sinA, n1*n3*delA+n2*sinA,
                     n2*n1*delA+n3*sinA, n2*n2*delA+cosA, n2*n3*delA-n1*sinA,
                     n3*n1*delA-n2*sinA, n3*n2*delA+n1*sinA, n3*n3*delA+cosA);

            for(int i = 0 ; i < points.size() ; i++){
                points.at(i) = rot*points.at(i);
            }
            forward = rot*forward;
            translate(o.p[0], o.p[1], o.p[2]);
        }

        void Player::rotateAroundOwnZ(double angle) {
            angle = angle * (3.141592635 / 180);
            double cA = currentZAngle + angle;
            if(cA == cA) {
                currentZAngle += angle;

                Vec3 o = origin;
                translate(-origin.p[0], -origin.p[1], -origin.p[2]);

                double n1 = forward.p[0];
                double n2 = forward.p[1];
                double n3 = forward.p[2];

                double delA = 1 - cos(angle);
                double cosA = cos(angle);
                double sinA = sin(angle);

                Mat3 rot(n1 * n1 * delA + cosA, n1 * n2 * delA - n3 * sinA, n1 * n3 * delA + n2 * sinA,
                         n2 * n1 * delA + n3 * sinA, n2 * n2 * delA + cosA, n2 * n3 * delA - n1 * sinA,
                         n3 * n1 * delA - n2 * sinA, n3 * n2 * delA + n1 * sinA, n3 * n3 * delA + cosA);

                for (int i = 0; i < points.size(); i++) {
                    points.at(i) = rot*points.at(i);
                }
                up = rot*up;
                right = rot*right;
                translate(o.p[0], o.p[1], o.p[2]);
            }
        }

        void Player::rotateAroundOwnX(double angle) {
            angle = angle * (3.141592635 / 180);
            currentXAngle += angle;
            Vec3 o = origin;
            translate(-origin.p[0],-origin.p[1],-origin.p[2]);

            double n1 = right.p[0];
            double n2 = right.p[1];
            double n3 = right.p[2];

            double delA = 1-cos(angle);
            double cosA = cos(angle);
            double sinA = sin(angle);

            Mat3 rot(n1*n1*delA+cosA, n1*n2*delA-n3*sinA, n1*n3*delA+n2*sinA,
                     n2*n1*delA+n3*sinA, n2*n2*delA+cosA, n2*n3*delA-n1*sinA,
                     n3*n1*delA-n2*sinA, n3*n2*delA+n1*sinA, n3*n3*delA+cosA);

            for(int i = 0 ; i < points.size() ; i++){
                points.at(i) = rot*points.at(i);
            }
            forward = rot*forward;
            translate(o.p[0], o.p[1], o.p[2]);
        }

        void Player::movePlayer(double xAngle, double yAngle, double zAngle, double speed){
            rotateAroundOwnZ(zAngle);
            if(currentZAngle < -0.1 ) {
                yRot = -currentZAngle*0.5;
                //rotateAroundOwnY(yRot);
            }else if(currentZAngle > 0.1) {
                yRot = -currentZAngle*0.5;
                //rotateAroundOwnY(yRot);
            }
            xRot = xAngle;
            if(xRot < 0 ){
                xRot *= -1;
            }
            Vec3 t = (up-origin)*xRot*0.1;
            translate(t.p[0],t.p[1],t.p[2]);
            rotateAroundOwnX(xAngle);
            moveForward(speed);
        }
}
