//
// Created by Julius Hetzel on 09.06.15.
//

#ifndef OPENGLSTARFOX_AREA_H
#define OPENGLSTARFOX_AREA_H

#include <stdio.h>
#include <GLFW/glfw3.h>
#include <vector>
#include "vec3.hpp"
namespace starfox {
    namespace graphics {
        class Area {
        private:
            double WIDTH = 20;
            double DEPTH = 20;

            void initializeGround();
            void giveAreaAngle(double angle);
        public:
            Area();

            void drawGround();
        };
    }
}

#endif //OPENGLSTARFOX_AREA_H
