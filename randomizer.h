//
// Created by Julius Hetzel on 09.06.15.
//

#ifndef OPENGLSTARFOX_RANDOMIZER_H
#define OPENGLSTARFOX_RANDOMIZER_H


#include <stdio.h>
#include <GLFW/glfw3.h>
#include <vector>
#include "vec3.hpp"

namespace starfox {
    namespace graphics {
        class Randomizer {
        private:
            std::vector<Vec3> givenPositions = *new std::vector<Vec3>;

            bool notEnoughSpaceToRestOfObjects(Vec3 &position, int distance);

            bool notEnoughSpace(Vec3 &positionA, Vec3 &positionB, int distance);

        public:
            Randomizer();

            void initializeRandomCoordinates(int amount, double WIDTH, double DEPTH, int distance);

            void drawRandomObjects(double radius, int amount, double WIDTH, double DEPTH, int distance);
        };
    }
}


#endif //OPENGLSTARFOX_RANDOMIZER_H
