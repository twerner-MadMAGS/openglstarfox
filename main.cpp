//
//  main.cpp
//  Starfox
//
//  Created by Julius Hetzel on 26.05.15.
//  Copyright (c) 2015 de.hsb.JUTOKE. All rights reserved.
//

#include <iostream>
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "globals.h"
#include "window.h"
#include "player.h"
#include "area.h"
#include "randomizer.h"
#include "Mat3.h"

double x_rot = 0;
double y_rot = 0;
double z_rot = 0;
static double rotSpeed_ = 0.8;
static double xAlpha_ = 0;
static double yAlpha_ = 0;
static double zAlpha_ = 0;
static double currentXAngle;
static double currentYAngle;
static double angle = 0.02;

static double scale_ = 0.1;

static double speed_ = 0.5;
Vec3 currentPos_ = *new Vec3(0,0,0.1);

static Vec3 spherePos(-4, 0, 0);
static double sphereRadius = 1.5;
double sphereXSpeed = 0;
double sphereYSpeed = 0;

static bool collosion = false;

static int checkColl = 0;
long threadID = 0;
double cCheckResolution = 0.3;
double cCheckSteps = 1 / cCheckResolution;

static starfox::Player player("Hans");

void SetMaterialColor(int side, double r, double g, double b) {
  float	amb[4], dif[4], spe[4];
  int mat;

  dif[0] = r;
  dif[1] = g;
  dif[2] = b;

  for(int i = 0; i < 3; i++) {
    amb[i] = .1 * dif[i];
    spe[i] = .5;
  }
  amb[3] = dif[3] = spe[3] = 1.0;

  switch(side){
    case 1:	mat = GL_FRONT;
      break;
    case 2:	mat = GL_BACK;
      break;
    default: mat = GL_FRONT_AND_BACK;
  }

  glMaterialfv(mat, GL_AMBIENT, amb);
  glMaterialfv(mat, GL_DIFFUSE, dif);
  glMaterialfv(mat, GL_SPECULAR, spe);
  glMaterialf( mat, GL_SHININESS, 20);
}


void DrawSphere(const Vec3& ctr, double r){
  int     i, j,
          n1 = 6, n2 = 12;
  Vec3    normal, v1;
  double  a1, a1d = M_PI / n1,
          a2, a2d = M_PI / n2,
          s1, s2,
          c1, c2;

  glShadeModel(GL_SMOOTH);
  for(i = 0; i < n1; i++){
    a1 = i * a1d;

    glBegin(GL_TRIANGLE_STRIP);
    for(j = 0; j <= n2; j++){
      a2 = (j + .5 * (i % 2)) * 2 * a2d;

      s1 = sin(a1);
      c1 = cos(a1);
      s2 = sin(a2);
      c2 = cos(a2);
      normal = c1 * XVec3 + s1 * (c2 * YVec3 + s2 * ZVec3);
      v1 = ctr + r * normal;
      glNormal3dv(normal.p);
      glVertex3dv(v1.p);

      s1 = sin(a1 + a1d);
      c1 = cos(a1 + a1d);
      s2 = sin(a2 + a2d);
      c2 = cos(a2 + a2d);
      normal = c1 * XVec3 + s1 * (c2 * YVec3 + s2 * ZVec3);
      v1 = ctr + r * normal;
      glNormal3dv(normal.p);
      glVertex3dv(v1.p);
    }
    glEnd();
  }
}

bool checkLineForCollosion(Vec3 start, Vec3 direction) {
	//fast-out Helper
	double oldDistance = 0;

	for(int i = 0; i <= cCheckSteps; i++) {
		double currentStep = cCheckResolution * i;
		Vec3 currentDirection = direction;
		currentDirection *= currentStep;
		Vec3 currentPoint = start;
		currentPoint += currentDirection;
		//get distance to sphere
		Vec3 distanceVec = spherePos - currentPoint;
		double distance = distanceVec.Length();
		if(distance <= sphereRadius) {
			collosion = true;
			return collosion;
		}
		//fast-out
		if(oldDistance == 0) {
			oldDistance = distance;
		}
		else if(distance > oldDistance){
			//if we move away from the sphere -> Abort
			collosion = false;
			return collosion;
		}

		Vec3 maxDistanceVec = start;
		maxDistanceVec += currentDirection;
		double maxDistance = maxDistanceVec.Length();
		if(maxDistance <= sphereRadius){
			//if we never reach the sphere -> Abort
			collosion = false;
			return collosion;
		}
	}

	collosion = false;
	return collosion;
}

void checkForCollosion(int phase) {


	/*
	for(int i = 0; i < player.trianglesAmount; i++) {
		starfox::sfTriangle currentTriangle = player.triangles[i];

		//check der Kanten
		if(phase == 1) {
			//A->B
			Vec3 start = currentTriangle.points[0];
			Vec3 direction = currentTriangle.points[1] - currentTriangle.points[0];
			checkLineForCollosion(start, direction);
		}
		else if(phase == 2) {
			//B->C
			Vec3 start = currentTriangle.points[1];
			Vec3 direction = currentTriangle.points[2] - currentTriangle.points[1];
			checkLineForCollosion(start, direction);
		}
		else if(phase == 3) {
			//C->A
			Vec3 start = currentTriangle.points[2];
			Vec3 direction = currentTriangle.points[0] - currentTriangle.points[2];
			checkLineForCollosion(start, direction);
		}
		//check der mitte
		else if(phase == 4) {
			//A->0.3BC
			Vec3 start = currentTriangle.points[0];
			Vec3 direction = currentTriangle.points[1] + ((currentTriangle.points[2] - currentTriangle.points[1]) * 0.3);
			direction = direction - start;
			checkLineForCollosion(start, direction);
		}
		else if(phase == 5) {
			//B->0.3CA
			Vec3 start = currentTriangle.points[1];
			Vec3 direction = currentTriangle.points[2] + ((currentTriangle.points[0] - currentTriangle.points[2]) * 0.3);
			direction = direction - start;
			checkLineForCollosion(start, direction);
		}
		else if(phase == 6) {
			//C->0.3AB
			Vec3 start = currentTriangle.points[2];
			Vec3 direction = currentTriangle.points[0] + ((currentTriangle.points[1] - currentTriangle.points[0]) * 0.3);
			direction = direction - start;
			checkLineForCollosion(start, direction);
		}
	}
	*/
}

void keyPressed(GLFWwindow* window, int key, int scancode, int action, int mods){
    if(action == GLFW_PRESS){
        switch(key){
            case GLFW_KEY_W: x_rot += rotSpeed_;
                break;
            case GLFW_KEY_S: x_rot -= rotSpeed_;
                break;
            case GLFW_KEY_A: z_rot -= rotSpeed_;
                //y_rot += rotSpeed_/2;
                break;
            case GLFW_KEY_D: z_rot += rotSpeed_;
                //y_rot -= rotSpeed_/2;
                break;
            case GLFW_KEY_T: scale_ += 0.1;
                break;
            case GLFW_KEY_Z: scale_ -= 0.1;
                break;
            case GLFW_KEY_UP: sphereYSpeed += 0.1;
                break;
            case GLFW_KEY_DOWN: sphereYSpeed -= 0.1;
                break;
            case GLFW_KEY_LEFT: sphereXSpeed -= 0.1;
                break;
            case GLFW_KEY_RIGHT: sphereXSpeed += 0.1;
                break;
        }
    }else if(action == GLFW_RELEASE){
        switch(key){
            case GLFW_KEY_W: x_rot = 0;
                break;
            case GLFW_KEY_S: x_rot = 0;
                break;
            case GLFW_KEY_A: z_rot = 0;
                y_rot = 0;
                break;
            case GLFW_KEY_D: z_rot = 0;
                y_rot = 0;
                break;
            case GLFW_KEY_UP: sphereYSpeed = 0;
                break;
            case GLFW_KEY_DOWN: sphereYSpeed = 0;
                break;
            case GLFW_KEY_LEFT: sphereXSpeed = 0;
                break;
            case GLFW_KEY_RIGHT: sphereXSpeed = 0;
                break;
        }
    }
}

void DrawSphere3(const Vec3 &ctr, double r) {
    int i, j,
            n1 = 20, n2 = 24; //resolution!!
    Vec3 normal, v1;
    double a1, a1d = M_PI / n1,
            a2, a2d = M_PI / n2,
            s1, s2,
            c1, c2;

    glShadeModel(GL_SMOOTH);
    for (i = 0; i < n1; i++) {
        a1 = i * a1d;

        glBegin(GL_TRIANGLE_STRIP);
        for (j = 0; j <= n2; j++) {
            a2 = (j + .5 * (i % 2)) * 2 * a2d;

            s1 = sin(a1);
            c1 = cos(a1);
            s2 = sin(a2);
            c2 = cos(a2);
            normal = c1 * XVec3 + s1 * (c2 * YVec3 + s2 * ZVec3);
            v1 = ctr + r * normal;
            glNormal3dv(normal.p);
            glVertex3dv(v1.p);

            s1 = sin(a1 + a1d);
            c1 = cos(a1 + a1d);
            s2 = sin(a2 + a2d);
            c2 = cos(a2 + a2d);
            normal = c1 * XVec3 + s1 * (c2 * YVec3 + s2 * ZVec3);
            v1 = ctr + r * normal;
            glNormal3dv(normal.p);
            glVertex3dv(v1.p);
        }
        glEnd();
    }
}

void InitLighting() {
  GLfloat lp1[4]  = { 10,  5,  10,  0};
  GLfloat lp2[4]  = { -5,  5, -10,  0};
  GLfloat red[4]  = {1.0, .8,  .8,  1};
  GLfloat blue[4] = { .8, .8, 1.0,  1};

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glClearColor(1, 1, 1, 1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glShadeModel(GL_SMOOTH);
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, 1);
  glEnable(GL_LIGHTING);

  glLightfv(GL_LIGHT1, GL_POSITION, lp1);
  glLightfv(GL_LIGHT1, GL_DIFFUSE,  red);
  glLightfv(GL_LIGHT1, GL_SPECULAR, red);
  glEnable(GL_LIGHT1);

  glLightfv(GL_LIGHT2, GL_POSITION, lp2);
  glLightfv(GL_LIGHT2, GL_DIFFUSE,  blue);
  glLightfv(GL_LIGHT2, GL_SPECULAR, blue);
  glEnable(GL_LIGHT2);

  glClearColor(1, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // init viewport to canvassize
  glViewport(0, 0, window_width_, window_height_);

  // init coordinate system
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-15, 15, -10, 10, -20, 20);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}


void Preview() {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    SetMaterialColor(0, 0, 1.0, 0);
    
    glPushMatrix();
    glScaled(0.2,0.2,0.2);

    spherePos.p[0] += sphereXSpeed;
    spherePos.p[1] += sphereYSpeed;

	checkForCollosion(checkColl);
    if(checkColl == 6) {
    	checkColl = 1;
    }
    else {
    	checkColl++;
    }

    DrawSphere(spherePos, sphereRadius);
    player.draw_body();

    player.movePlayer(xAlpha_,0.0,zAlpha_, speed_);
    glTranslated(0,0,player.getZValue());
    player.draw_body();

    glPopMatrix();

    xAlpha_ = x_rot;
    yAlpha_ = y_rot;
    zAlpha_ = z_rot;

    glEnd();

}

void Test(){
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    glTranslated(0,200,0);
    DrawSphere3(*new Vec3(0,0,0),2);
    glPopMatrix();
    glEnd();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glPushMatrix();
    glTranslated(0,20,0);
    glPopMatrix();
}


int main() {
    GLFWwindow* window = NULL;
    
    printf("Here we go!\n");
    
    if(!glfwInit()){
        return -1;
    }
    
    window = glfwCreateWindow(window_width_, window_height_,
                              "Simple 3D Animation", NULL, NULL);
    if(!window) {
        glfwTerminate();
        return -1;
    }


    glfwMakeContextCurrent(window);
    while(!glfwWindowShouldClose(window)) {
        
        // switch on lighting (or you don't see anything)
        InitLighting();
        glfwSetKeyCallback(window, keyPressed);
        
        // set background color
        if(collosion) {
            glClearColor(150, 0, 0, 1.0);
        }
        else {
            glClearColor(0.8, 0.8, 0.8, 1.0);
        }
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // draw the scene
        Preview();
        //Test();
        // make it appear (before this, it's hidden in the rear buffer)
        glfwSwapBuffers(window);
        
        glfwPollEvents();
    }
    
    glfwTerminate();
    
    printf("Goodbye!\n");
    return 0;
}
