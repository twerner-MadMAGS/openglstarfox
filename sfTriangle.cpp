//
//  player.cpp
//  Starfox
//
//  Created by Julius Hetzel on 26.05.15.
//  Copyright (c) 2015 de.hsb.JUTOKE. All rights reserved.
//

#include "sfTriangle.h"


namespace starfox {
    
	sfTriangle::sfTriangle(Vec3 p1, Vec3 p2, Vec3 p3, Vec3 norm){
		points.push_back(p1);
		points.push_back(p2);
		points.push_back(p3);
		normal = norm;
	}

	sfTriangle::~sfTriangle(){
	}
    
    void sfTriangle::draw() {
    	glBegin(GL_TRIANGLES);
    	//TODO
        glEnd();
    }
}
