//
// Created by Julius Hetzel on 09.06.15.
//


#include "area.h"
#include "randomizer.h"



namespace starfox {
    namespace graphics {
        Area::Area() {

        }

        void Area::drawGround() {
            giveAreaAngle(5.0);
        }

        void Area::initializeGround(){
            glBegin(GL_QUADS);
            glVertex3d(-WIDTH/2, -20, -DEPTH);
            glVertex3d(-WIDTH/2, -20, 0);
            glVertex3d(WIDTH/2, -20, 0);
            glVertex3d(WIDTH/2, -20, -DEPTH);
            glEnd();
        }

        void Area::giveAreaAngle(double angle){
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            glRotated(angle, 1, 0, 0);
            initializeGround();
            Randomizer rndObjects = *new Randomizer();
            rndObjects.drawRandomObjects(2.0,10,WIDTH,DEPTH,2.0);
            glPopMatrix();
        }
    }
}
