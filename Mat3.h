//
// Created by Julius Hetzel on 14.06.15.
//

#ifndef OPENGLSTARFOX_MAT3_H
#define OPENGLSTARFOX_MAT3_H


#include "vec3.hpp"

class Mat3 {
public:
    double	p[3][3];

    Mat3();
    Mat3(double m[3][3]);
    Mat3(double a0, double a1,double a2,double a3,double a4,double a5,double a6,double a7,double a8);

    void printMat();
    Mat3& operator*=(Mat3 a);
    inline friend Mat3 operator*(Mat3 a, Mat3 b) {
        return a *= b;
    }

    Vec3 operator*=(Vec3& v);
    inline friend Vec3 operator*(Mat3 a, Vec3 b) {
        return a *= b;
    }
};

#endif //OPENGLSTARFOX_MAT3_H
