//
//  constants.h
//  Starfox
//
//  Created by Julius Hetzel on 26.05.15.
//  Copyright (c) 2015 de.hsb.JUTOKE. All rights reserved.
//
#define DEBUG 1

#if DEBUG
#define LOG(x) std::cout << x << std::endl
#endif

#ifndef Starfox_constants_h
#define Starfox_constants_h

const static int window_width_ = 1024;
const static int window_height_ = 768;



#endif
