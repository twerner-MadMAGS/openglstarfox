//
// Created by Julius Hetzel on 09.06.15.
//

#include "randomizer.h"
#include <cmath>
#include <iostream>

namespace starfox {
    namespace graphics {
        Randomizer::Randomizer(){

        }

        void Randomizer::initializeRandomCoordinates(int amount, double WIDTH, double DEPTH, int distance) {
            for (int i = 0; i < amount; i++) {

                double x = -WIDTH + ((double) rand() / RAND_MAX) * WIDTH * 2;
                double y = -20;
                double z = -DEPTH + ((double) rand() / RAND_MAX) * DEPTH * 2;

                Vec3 newPosition = *new Vec3(x, y, z);

                while (notEnoughSpaceToRestOfObjects(newPosition, distance)) {
                    x = -WIDTH + ((double) rand() / WIDTH) * WIDTH * 2;
                    y = -20;
                    z = -DEPTH + ((double) rand() / DEPTH) * DEPTH * 2;
                    newPosition = *new Vec3(x, y, z);
                }

                givenPositions.push_back(newPosition);
            }
        }

        void Randomizer::drawRandomObjects(double radius, int amount, double WIDTH, double DEPTH, int distance) {
            initializeRandomCoordinates(amount, WIDTH, DEPTH, distance);
            for (int i = 0; i < givenPositions.size(); i++) {
                //DrawSphere(givenPositions.at(i), radius);
                char *k;
                givenPositions.at(i).Print(k);
                std::cout << &k;
            }
        }

        bool Randomizer::notEnoughSpaceToRestOfObjects(Vec3 &position, int distance) {
            for (int i = 0; i < givenPositions.size(); i++) {
                if (notEnoughSpace(givenPositions.at(i), position, distance)) {
                    return true;
                }
            }
            return false;
        }

        bool Randomizer::notEnoughSpace(Vec3 &positionA, Vec3 &positionB, int distance) {
            double xA = positionA.getCoordinate(0);
            double zA = positionA.getCoordinate(2);

            double xB = positionB.getCoordinate(0);
            double zB = positionB.getCoordinate(2);

            if (fabs(xA - xB) < distance || fabs(zA - zB) < distance) {
                return true;
            }
            return false;
        }
    }
}
